package com.devcher.bookstore.books;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class BooksApplicationTests
{
	@Autowired
	private BookController bookController;

	@Autowired
	private BookRepository bookRepository;

	@Test
	public void contextLoads()
	{
		assertThat(bookController).isNotNull();
		assertThat(bookRepository).isNotNull();
	}
}

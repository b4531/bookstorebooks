package com.devcher.bookstore.books;

import com.devcher.bookstore.books.models.Author;
import com.devcher.bookstore.books.models.Book;
import com.devcher.bookstore.books.models.Theme;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class BookRepository
{
	private List<Author> authors = new ArrayList<>(Arrays.asList(
			new Author(0L, "Марк Твен"),
			new Author(1L, "Брайан Трейси"),
			new Author(2L, "Ричард Брэнсон")
	));

	private List<Theme> themes = new ArrayList<>(Arrays.asList(
			new Theme(0L, "Психология"),
			new Theme(1L, "Приключение"),
			new Theme(2L, "Бизнес-литература")
	));

	private List<Book> books = new ArrayList<>(Arrays.asList(
			new Book(0L, "Мастер времени", "Книга о планировании.", 600, "null", authors.get(1), themes.get(0)),
			new Book(1L, "К черту все, берись и делай", "Биография великого предпринимателя Ричарда Брэнсона.", 1000, "null", authors.get(2), themes.get(2)),
			new Book(2L, "Приключения Тома Сойера", "Детский рассказ о веселом мальчике.", 400, "null", authors.get(0), themes.get(1))
	));

	public List<Book> findAll()
	{
		return books;
	}

	public void save(Book book)
	{
		books.add(book);
	}

	public Book findById(Long id)
	{
		return books.get(id.intValue());
	}

	public void deleteById(Long id)
	{
		books.remove(id.intValue());
	}
}
